import express = require('express');
import path = require('path');
import sequelize = require('./database/sq');
// import apiRouter = require('./routers/api');

var port: number = process.env.PORT || 3000;
var app = express();


app.use('/app', express.static(path.resolve(__dirname, 'app')));
app.use('/libs', express.static(path.resolve(__dirname, 'libs')));

var renderIndex = (req: express.Request, res: express.Response) => {
    res.sendFile(path.resolve(__dirname, 'index.html'));
}

app.get('/app/*', renderIndex);

var server = app.listen(port, function() {
    var host = server.address().address;
    var port = server.address().port;
    console.log('This express app is listening on port:' + port);
});

// app.use('/api', apiRouter);

