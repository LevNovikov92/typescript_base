import {Component, OnInit} from 'angular2/core';

export class Hero {
    id: number;
    name: string;
}

@Component({
    selector: 'hero',
    template: 
        `<h1>Hero component</h1>      
        <h3>{{hero.name}}</h3>
        <p>Hero id: {{hero.id}}</p>`
})

export class HeroComponent implements OnInit {
    constructor() { }

    ngOnInit() { }

    hero: Hero = {
        id: 1,
        name: "Wind"
    }
}